import React, { Component } from "react";
import TapView from "./TapView";

class TapContainer extends Component {
  state = {
    data: [],
    progress: [],
    finished: []
  };

  onRowSelect = id => {
    console.log(id);
  };
  render() {
    this.state = {
      data: this.props.data
    };
    const { data } = this.state;
    const { onRowSelect, progress, finished } = this.props;
    return (
      <React.Fragment>
        <TapView
          data={data}
          onRowSelect={onRowSelect}
          progress={progress}
          finished={finished}
        />
      </React.Fragment>
    );
  }
}

export default TapContainer;
