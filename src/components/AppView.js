import React from "react";
import App from "./App.css";
import HeaderContainer from "../components/header/HeaderContainer";
import TapContainer from "../components/tap/TapContainer";
function AppView(props) {
  const {
    onNewTask,
    data,
    model,
    onRowSelect,
    onSetValueToState,
    onRowDelete,
    onSetValueAction,
    progress,
    finished
  } = props;

  return (
    <React.Fragment>
      <div className="container-fluid">
        <div className="row d-flex justify-content-center">
          <div className="col-7 contendor">
            <HeaderContainer
              data={data}
              onSetValueToState={onSetValueToState}
              onSetValueAction={onSetValueAction}
              model={model}
            />
            <TapContainer
              progress={progress}
              finished={finished}
              data={data}
              onNewTask={onNewTask}
              onRowDelete={onRowDelete}
              onRowSelect={onRowSelect}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

export default AppView;
