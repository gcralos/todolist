import React, { Component } from "react";
import AppView from "./AppView";
import { resolve } from "dns";
class App extends Component {
  //Inicilizacion
  state = {
    idRow: [],
    data: [],
    progress: [],
    finished: []
  };

  //funciones
  onSetValueToState = model => {
    console.log(model)
    this.setState({
      data: [
        ...this.state.data,
        {
          id: Date.now(),
          name: model.nametask,
          descripcion: model.descriptiontask,
          key: Date.now()
        }
      ]
    });
  };
  onSetValueAction = ({ name }) => {
    const model = this.state.idRow;
    const modelNewState = [...this.state.data];
    model.map(item => {
      const arrayuno = modelNewState.filter(itendos => {
        return itendos.id !== item.id;
      });
      this.setState({
        data: arrayuno
      });
    });

    switch (name) {
      case "IN_PROSGRESS":
        this.setState({
          progress: [...model]
        });
        break;

      default:
        break;
    }
  };
  onNewTask=()=>{
  }
  onRowSelect = id => {
    this.setState({
      idRow: id
    });
  };
  //fin funciones
  render() {
    const { data, idRow, progress, finished, onNewTask } = this.state;
    return (
      <React.Fragment>
        <AppView
          onNewTask={onNewTask}
          progress={progress}
          data={data}
          onSetValueToState={this.onSetValueToState}
          onRowSelect={this.onRowSelect}
          onSetValueAction={this.onSetValueAction}
        
        ></AppView>
      </React.Fragment>
    );
  }
}

export default App;
