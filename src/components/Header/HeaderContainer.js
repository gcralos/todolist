import React, { Component } from "react";
import HeaderView from "./HeaderView";
import { identifier } from "@babel/types";

export class HeaderContainer extends Component {
  state={
      nametask:'',
      descriptiontask:''
  }
  componentWillUnmount() {/**se invoca inmediatamente antes de desmontar y destruir un componente. */}
  componentDidUpdate(prevProps, prevState, snapshot) {/**se invoca inmediatamente después de que la actualización ocurra.*/}
  componentDidMount() {/*se invoca inmediatamente después de que un componente se monte*/}
  handelChange = ( { name, value}) =>{
     this.setState (()=>{
         return{ [name]:value }
     })
     
  }
  onNewTask = ()=>{
    const mode = { ...this.state }
    this.props.onSetValueToState(mode);
  }
  render() {
    const { onSetValueToState,onSetValueAction,onNewTask } = this.props;
    const  model = { ...this.state }
     console.log(model)
    return (
      <React.Fragment>
        <HeaderView
          handelChange={this.handelChange}
          onNewTask={this.onNewTask}
          onSetValueAction={onSetValueAction}
          onSetValueToState= {onSetValueToState}
          model = {model }
        />
      </React.Fragment>
    );
  }
}

export default HeaderContainer;
