import React, { Component } from "react";
import TodoListView from "./TodoListView";

class TodoListContainer extends Component {
  state={
     data:''
  }
  render() {
    const { data, onRowSelect, progress, finished } = this.props;
    return (
      <React.Fragment>
        <TodoListView
          data={data}
          onRowSelect={onRowSelect}
          progress={progress}
          finished={finished}
        />
      </React.Fragment>
    );
  }
}

export default TodoListContainer;
